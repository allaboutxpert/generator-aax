(function() {
    'use strict';

    angular
        .module('app.controllers')
        .controller('HeaderController', HeaderController);

    HeaderController.$inject = [];

    function HeaderController () {
        var vm = this;

        vm.showMenu = false;

        vm.toggleMobileMenu = toggleMobileMenu;

        function toggleMobileMenu() {
            vm.showMenu = !vm.showMenu;
        }
        //
    }

})();
