(function () {
    'use strict';

    angular
        .module('app.routes')
        .config(OntimeRegistrationRouter);

    OntimeRegistrationRouter.$inject = ['$stateProvider'];
    /* @ngInject */
    function OntimeRegistrationRouter($stateProvider) {
        $stateProvider
            .state('app.ontimeregistration', {
                url: '/ontimeregistration',
                data: {},
                views: {
                    'main@': {
                        templateUrl: '/features/ontimeregistration/ontimeregistration.html',
                        controller: 'OntimeRegistrationController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    // Constant title
                    $title: function() { return 'OntimeRegistration'; }
                }
            });
    }

})();
