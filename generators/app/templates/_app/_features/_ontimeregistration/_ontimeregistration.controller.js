(function () {
    'use strict';

    angular
        .module('app.controllers')
        .controller('OntimeRegistrationController', OntimeRegistrationController);

    OntimeRegistrationController.$inject = ['$q', '$location', 'API', 'logger'];

    function OntimeRegistrationController ($q, $location, API, logger) {
        var vm = this;

        vm.token = null;
        vm.location = $location.search();
        vm.onTimeTokenUrl = 'http://ontime-external.allaboutxpert.com/api/oauth2/token?' +
            'grant_type=authorization_code' +
            '&client_id=d751de94-36db-464e-9a32-dd6de90ada7c' +
            '&redirect_uri=http://localhost:8080/ontimeregistration' +
            '&client_secret=3e32af93-d67f-4cff-8ea5-0220cf0fe8f6' +
            '&code=';

        vm.getToken = getToken;

        //If OnTime Authorization code available in URL retrieve token from OnTime
        if (vm.location.hasOwnProperty('code')) {
            vm.getToken(vm.location.code).then(function(token) {
                var tmpToken = {
                    accessToken: token.data.access_token, // jshint ignore:line
                    tokenType: token.data.token_type,     // jshint ignore:line
                    data: token.data.data
                };

                vm.token = angular.fromJson(tmpToken);
                localStorage.setItem('OnTimeToken', JSON.stringify(vm.token));
                vm.message = 'Successfully registered with OnTime!';
                logger.info(vm.message, null, 'Success');
            });
        } else {
            vm.message = 'Error registering with OnTime';
            logger.warning(vm.message, null, 'Warning');
        }

        //API call to retrieve Access Token from OnTime
        function getToken(code) {
            var deferred = $q.defer();

            API.oneUrl('ontime', vm.onTimeTokenUrl + code).get()
                .then(function (ontime) {
                        deferred.resolve(ontime);
                    }, function () {
                        deferred.reject(null);
                    }
                );
            return deferred.promise;
        }
        //
    }

})();
