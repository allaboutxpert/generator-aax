(function () {
    'use strict';

    angular
        .module('app.routes')
        .config(LandingRouter);

    LandingRouter.$inject = ['$stateProvider'];
    /* @ngInject */
    function LandingRouter($stateProvider) {
        $stateProvider
            .state('app.landing', {
                url: '/',
                data: {},
                views: {
                    'main@': {
                        templateUrl: '/features/landing/landing.html',
                        controller: 'LandingController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    // Constant title
                    $title: function () {
                        return 'Home';
                    }
                }
            });
    }

})();
