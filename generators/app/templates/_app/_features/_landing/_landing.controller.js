(function () {
    'use strict';

    angular
        .module('app.controllers')
        .controller('LandingController', LandingController);

    LandingController.$inject = ['$q','logger', 'API'];

    function LandingController($q, logger, API) {
        var vm = this;

        //---                        START EXAMPLE CODE                     ---//
        // Not efficient or for production use, only to serve as a quick
        // introduction to the Generated APP
        //
        // You will notice that API has been injected into this Controller
        // API is a wrapper service to Restangular that we will use to make
        // all API calls.
        //
        // Please see https://github.com/mgonto/restangular for more info
        //
        // We will access the OnTime API with the Restangular API service,
        // You can learn more about the OnTime API at
        // http://developer.axosoft.com/api.html
        //
        // The following additional feature and Config were created in order
        // to make it work in this example and can be safely deleted if you
        // don't use the OnTime API
        //
        // ontime.config.js
        //   This ads a Restangular ResponseInterceptor to format the OnTime
        //   result set so it can be properly used with Restangular
        //
        // ontimeregistration
        //   This feature is mainly used to retrieve the OnTime Access Token
        //   from OnTime and persist it in local storage.
        //
        //---------------------------------------------------------------------//

        //Prepare default message to the user that will show in the toastr
        vm.message = 'hello stranger';

        //OnTime auth URL to link OnTime user to this app
        //If no OnTime token is retrieved in the next step,
        //a link to OnTime button will be displayed refrencing this URL
        vm.onTimeUrl = 'http://ontime-external.allaboutxpert.com/auth?' +
            'response_type=code&client_id=d751de94-36db-464e-9a32-dd6de90ada7c' +
            '&redirect_uri=http://localhost:8080/ontimeregistration' +
            '&scope=read%20write' +
            '&expiring=false';

        //Retrieve OnTime token from local storage and parse string to object
        vm.onTimeToken = JSON.parse(localStorage.getItem('OnTimeToken'));

        //Define getWork function
        vm.getWork = getWork;

        //If user is linked to OnTime retrieve all his work from OnTime and assign to vm.work
        if (vm.onTimeToken) {

            //Set the request params
            var requestParams = {
                access_token: vm.onTimeToken.accessToken,  // jshint ignore:line
                assigned_to_id: vm.onTimeToken.data.id     // jshint ignore:line
            };

            API.setDefaultRequestParams(requestParams);

            //Set the base URL for ontime API
            API.setBaseUrl('http://ontime-external.allaboutxpert.com/api/v5');

            //Get a promise to the OnTime Data and process when promise is resolved
            vm.getWork().then(function(work) {
                vm.work = work;

                //Prepare personalized message to user
                vm.message = 'Hello ' +
                    vm.onTimeToken.data.first_name + // jshint ignore:line
                    ' ' +
                    vm.onTimeToken.data.last_name;   // jshint ignore:line

                //Display a tostr and log the message to the console
                logger.info(vm.message, null, 'Test Message');

            });
        }

        //Api Call to retrieve calls from OnTime
        function getWork() {
            var deferred = $q.defer();

            API.all('items').getList()
                .then(function (items) {
                        deferred.resolve(items);
                    }, function () {
                        deferred.reject(null);
                    }
                );
            return deferred.promise;
        }

        //---                        END EXAMPLE CODE                       ---//

    }
})();
