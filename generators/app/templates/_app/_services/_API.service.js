/*jshint -W089 */
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('API', API);

    API.$inject = ['Restangular', 'logger', '$localStorage'];

    function API(Restangular, logger, $localStorage) {

        //content negotiation
        var headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/x.laravel.v1+json'
        };

        return Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer
                .setBaseUrl()
                .setDefaultHeaders(headers)
                .setErrorInterceptor(function (response) {
                    if (response.status === 422) {
                        for (var error in response.data.errors) {
                            return logger.error(
                                response.data.errors[error][0],
                                response.data, 'Service Error'
                            );
                        }
                    }
                })
                .addFullRequestInterceptor(function (element, operation, what, url, headers) {
                    if ($localStorage.jwt) {
                        headers.Authorization = 'Bearer ' + $localStorage.jwt;
                    }
                });
        });
    }

})();
