/**
 * Created by linosteenkamp on 04 Mar 16.
 */
(function () {
    'use strict';

    angular.module('app.filters').filter('htmlToPlainText', function() {
        return function (input) {
            return (input) ? String(input).replace(/<[^>]+>|&(.*?);/gm, '') : '';
        };
    });
})();

