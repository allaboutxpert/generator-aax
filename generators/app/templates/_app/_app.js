(function () {
    'use strict';

    angular.module('app',
        [
            'app.templates',
            'app.controllers',
            'app.filters',
            'app.services',
            'app.directives',
            'app.config',
            'app.routes'
        ]);
    angular.module('app.routes',
        [
            'ui.router',
            'ui.router.title'
        ]);
    angular.module('app.controllers',
        [
            'ngStorage',
            'restangular',
            'angular-loading-bar',
            'app.services'
        ]);
    angular.module('app.filters', []);
    angular.module('app.services', ['toastr']);
    angular.module('app.directives', []);
    angular.module('app.config', []);
})();
