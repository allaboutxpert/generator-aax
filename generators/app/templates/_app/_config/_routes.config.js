(function () {
    'use strict';

    angular
        .module('app.routes')
        .config(router);

    router.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

    function router($stateProvider, $urlRouterProvider, $locationProvider) {

        $urlRouterProvider.otherwise('404');

        $stateProvider
            .state('app', {
                abstract: true,
                views: {
                    header: {
                        templateUrl: '/features/header/header.html'
                    },
                    footer: {
                        templateUrl: '/features/footer/footer.html'
                    },
                    main: {}
                }
            })
            .state('app.404', {
                url: '/404',
                data: {},
                views: {
                    'main@': {
                        templateUrl: '/404.html'
                    }
                },
                resolve: {
                    // Constant title
                    $title: function () {
                        return '404';
                    }
                }
            });

        // @if ENV == 'PRODUCTION'
        $locationProvider.html5Mode(true);
        // @endif
        // @if ENV == 'DEV'
        $locationProvider.html5Mode(true);
        // @endif

    }
})();
