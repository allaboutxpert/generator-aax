(function () {
    'use strict';

    angular
        .module('app.config')
        .config(loadingBar);

    loadingBar.$inject = ['cfpLoadingBarProvider'];

    function loadingBar(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = false;
    }

})();
