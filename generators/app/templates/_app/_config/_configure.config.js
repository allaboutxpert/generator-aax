(function () {
    'use strict';

    var config = {
        appErrorPrefix: '[wwwaax Error] '
    };

    angular
        .module('app.config')
        .value('config', config)
        .config(configure);

    configure.$inject = ['$logProvider', 'exceptionHandlerProvider'];
    /* @ngInject */
    function configure($logProvider, exceptionHandlerProvider) {
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
        exceptionHandlerProvider.configure(config.appErrorPrefix);
    }

})();
