/* jshint -W117 */
(function () {
    'use strict';

    angular
        .module('app.config')
        .config(config);

    config.$inject = ['toastrConfig'];

    function config(toastrConfig) {
        angular.extend(toastrConfig, {
            extendedTimeOut: 4000,
            positionClass: 'toast-bottom-right',
        });
    }

})();
