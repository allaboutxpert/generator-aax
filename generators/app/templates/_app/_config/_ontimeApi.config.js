/**
 * Created by lino on 03/03/2016.
 */
(function () {

    'use strict';

    angular
        .module('app.config')
        .config(ontimeApi);

    ontimeApi.$inject = ['RestangularProvider'];

    function ontimeApi(RestangularProvider) {
        // add a response intereceptor
        RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response) {
            var extractedData;
            var myUrl = url.split('/');
            // .. to look for getList operations
            if (operation === 'getList' && myUrl[2] === 'ontime-external.allaboutxpert.com') {
                // .. and handle the data and meta data
                extractedData = data.data;
            } else {
                extractedData = response;
            }
            return extractedData;
        });

    }
})();
