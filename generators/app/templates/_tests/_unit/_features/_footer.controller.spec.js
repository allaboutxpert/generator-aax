/**
 * Created by linosteenkamp on 01 Mar 16.
 */


describe('Footer Controller ', function () {

    beforeEach(module('app.controllers'));

    var vm,
        scope;

    beforeEach(inject(function ($rootScope, $controller) {
        scope = $rootScope.$new();
        vm = $controller('FooterController', {$scope : scope});
    }));

    describe('Activate ', function() {
        it('Should be created successfully', function () {
            expect(vm).toBeDefined();
        });
    });
});
