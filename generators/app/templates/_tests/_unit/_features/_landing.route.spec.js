/**
 * Created by lino on 01/03/2016.
 */
/* jshint -W117, -W030 */
'use strict';

describe('Application routes', function() {
    beforeEach(module('app.routes'));

    // Suite for testing an individual piece of our feature.
    describe('App Abstract Route', function () {

        // Instantiate global variables (global to all tests in this describe block).
        var $state,
            $rootScope,
            $httpBackend,
            state = 'app.landing';

        // Inject dependencies
        beforeEach(inject(function (_$state_, $templateCache, _$rootScope_, _$httpBackend_) {
            $state = _$state_;
            $rootScope = _$rootScope_;
            $templateCache.put('/features/landing/landing.html', '');
            $httpBackend = _$httpBackend_;
            $httpBackend.whenGET('/features/header/header.html').respond(200);
            $httpBackend.whenGET('/features/footer/footer.html').respond(200);
        }));

        // Test whether the url is correct
        it('should respond to URL', function() {
            expect($state.href(state)).toEqual('/');
        });

        it('verifies state configuration', function () {
            $state.go(state);
            $httpBackend.when('GET', '/features/landing/landing.html').respond(200);
            $rootScope.$apply();
            $httpBackend.flush();
            expect($state.current.name).toBe(state);
        });
    });
});
