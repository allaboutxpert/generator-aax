/**
 * Created by lino on 29/02/2016.
 */

describe('Landing Controller', function () {

    beforeEach(module('app.controllers'));

    var LandingController,
        scope;

    beforeEach(inject(function ($rootScope, $controller) {
        scope = $rootScope.$new();
        LandingController = $controller('LandingController', {$scope : scope});
    }));

    it('says hello world', function () {
        expect(LandingController.message).toBe('hello world');
    });
});
