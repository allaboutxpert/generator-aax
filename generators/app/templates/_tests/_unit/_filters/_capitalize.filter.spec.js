/**
 * Created by lino on 01/03/2016.
 */
/* jshint -W117, -W030 */
'use strict';

describe('capitalize filter', function() {

    var $filter;

    beforeEach(module('app.filters'));
    beforeEach(inject(function(_$filter_) {
        $filter = _$filter_;
    }));

    it('returns empty string when given null', function() {
        var capitalize = $filter('capitalize');
        expect(capitalize(null)).toBe('');
    });

    it('returns the correct value when given a string of chars', function() {
        var capitalize = $filter('capitalize');
        expect(capitalize('abc def')).toBe('Abc Def');
    });
});
