/**
 * Created by lino on 01/03/2016.
 */
/* jshint -W117, -W030 */
'use strict';

describe('ucfirst filter', function() {

    var $filter;

    beforeEach(module('app.filters'));
    beforeEach(inject(function(_$filter_) {
        $filter = _$filter_;
    }));

    it('returns null when given empty string', function() {
        var ucfirst = $filter('ucfirst');
        expect(ucfirst('')).toBe(null);
    });

    it('returns the correct value when given a string of chars', function() {
        var ucfirst = $filter('ucfirst');
        expect(ucfirst('abc def')).toBe('Abc def');
    });
});
