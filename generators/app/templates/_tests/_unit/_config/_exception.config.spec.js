/**
 * Created by lino on 01/03/2016.
 */
/* jshint -W117, -W030 */
'use strict';

describe('exception', function () {
    var mocks = {
        errorMessage: 'fake error',
        prefix: '[TEST]: '
    },
        $scope,
        exceptionHandlerProvider;

    beforeEach(module('app.controllers'));
    beforeEach(module('app.config'));
    beforeEach(module(function(_exceptionHandlerProvider_) {
        exceptionHandlerProvider = _exceptionHandlerProvider_;
    }));

    beforeEach(inject(function (_$rootScope_) {
        $scope = _$rootScope_;
    }));

    describe('exceptionHandlerProvider', function() {

        it('should be true', function () {
            expect(true).toBe(true);
        });

        it('should have exceptionHandlerProvider defined', function() {
            expect(exceptionHandlerProvider).toBeDefined();
        });

        it('should have configuration', function() {
            expect(exceptionHandlerProvider.config).toBeDefined();
        });

        describe('with appErrorPrefix', function() {
            beforeEach(function() {
                exceptionHandlerProvider.configure(mocks.prefix);
            });

            it('should have appErrorPrefix defined', function() {
                expect(exceptionHandlerProvider.$get().config.appErrorPrefix).toBeDefined();
            });

            it('should have appErrorPrefix set properly', function() {
                expect(exceptionHandlerProvider.$get().config.appErrorPrefix)
                    .toBe(mocks.prefix);
            });

            it('should throw an error when forced', function() {
                expect(functionThatWillThrow).toThrow();
            });

            it('manual error is handled by decorator', function() {
                exceptionHandlerProvider.configure(mocks.prefix);
                try {
                    $scope.$apply(functionThatWillThrow);
                }
                catch (ex) {
                    expect(ex.message).toBe(mocks.prefix + mocks.errorMessage);
                }
            });
        });
    });

    function functionThatWillThrow() {
        throw new Error(mocks.errorMessage);
    }
});
