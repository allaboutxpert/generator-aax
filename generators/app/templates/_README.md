This project is a starting point for aaX AngularJS projects using the [Gulp](http://gulpjs.com/) streaming build system. Almost everything important is in [gulpfile.js](https://github.com/paislee/healthy-gulp-angular/blob/master/gulpfile.js).

## Installation

Before running any Gulp tasks:

1. Ensure you have node installed
2. Run `npm install` in the project directory
3. Run `npm install -g karma-cli` in the project directory
4. Run `bower install` in the project directory
5. For livereload functionality, install the [livereload Chrome extension](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei)

## Gulp Tasks

All of the following are available from the command line.

### Essential ones

These tasks I use as part of my regular developments and deploy scripts:

- __`gulp`__ Default task runs watch-dev.
- __`gulp watch-dev`__ Clean, build, and watch live changes to the dev environment. Built sources are served directly by the dev server from .dev
- __`gulp build-dev`__ Build to the dev environment to .dev
- __`gulp watch-prod`__ Clean, build, and watch live changes to the prod environment. Built sources are served directly by the dev server from .prod
- __`gulp build-prod`__ Build to the prod environment to .prod
