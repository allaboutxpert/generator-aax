var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

var port = process.env.PORT || 8080; // set our port
var staticdir = process.env.NODE_DIR; //NODE_ENV === 'PRODUCTION' ? '.prod' : '.dev'; // get static files dir

// get all data/stuff of the body (POST) parameters
app.use(bodyParser.json()); // parse application/json

// override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(methodOverride('X-HTTP-Method-Override'));

// set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/' + staticdir));

app.use(function (req, res) {
    res.sendFile(path.join(__dirname, staticdir, 'index.html'));
});

// routes ==================================================
require('./devServer/routes')(app); // configure our routes

// start app ===============================================
app.listen(port);                   // startup our app at http://localhost:8080
console.log('Starting sever on port ' + port);       // shoutout to the user

// expose app
exports = module.exports = app;
