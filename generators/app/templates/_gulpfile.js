var gulp = require('gulp');
var gulpAngularTplCache = require('gulp-angular-templatecache');
var gulpLivereload = require('gulp-livereload');
var gulpPreprocess = require('gulp-preprocess');
var gulpRemember = require('gulp-remember');
var gulpHtmlhint = require('gulp-htmlhint');
var gulpFlatten = require('gulp-flatten');
var gulpCssnano = require('gulp-cssnano');
var gulpHtmlmin = require('gulp-htmlmin');
var gulpNodemon = require('gulp-nodemon');
var gulpCsslint = require('gulp-csslint');
var gulpFilter = require('gulp-filter');
var gulpInject = require('gulp-inject');
var gulpUglify = require('gulp-uglify');
var gulpJshint = require('gulp-jshint');
var gulpConcat = require('gulp-concat');
var gulpCache = require('gulp-cached');
var gulpOrder = require('gulp-order');
var gulpJscs = require('gulp-jscs');
var gulpIf = require('gulp-if');
var bowerFiles = require('main-bower-files');
var lazypipe = require('lazypipe');
var del = require('del');
var es = require('event-stream');
var Q = require('q');

// == GLOBAL VARIABLES ========
var distFolder = '.dev';
var env = 'DEV';

// == PATH STRINGS ========
var appPaths = {
    base: './app',
    images: './app/images',
    fonts: './bower_components',
    styles: './app/styles',
    theme: './bootstrap_theme',
    index: './app/index.html'
};

var destPaths = {
    base: distFolder,
    images: distFolder + '/images',
    fonts: distFolder + '/fonts',
    scripts: distFolder + '/scripts',
    styles: distFolder + '/styles',
    index: distFolder + '/index.html'
};

// == PIPE SEGMENTS ========
var pipes = {};

pipes.buildVendorStyles = function ()
{
    var scriptChanel = lazypipe()
        .pipe(gulpOrder, ['bootstrap.css'])
        .pipe(function() {
            return gulpIf(env === 'PRODUCTION', gulpCssnano());
        })
        .pipe(function() {
            return gulpIf(env === 'PRODUCTION', gulpConcat('vendor.min.css'));
        });

    return gulp.src(bowerFiles())
        .pipe(gulpFilter('*.css'))
        .pipe(gulpFlatten())
        .pipe(scriptChanel())
        .pipe(gulp.dest(destPaths.styles));
};

pipes.buildThemeStyles = function ()
{
    var scriptChanel = lazypipe()
        .pipe(gulpOrder, ['css', 'styles.css', 'theme*.css'])
        .pipe(function() {
            return gulpIf(env === 'PRODUCTION', gulpCssnano());
        })
        .pipe(function() {
            return gulpIf(env === 'PRODUCTION', gulpConcat('theme.min.css'));
        });

    return gulp.src(appPaths.theme + '/**/*.css')
        .pipe(gulpFlatten())
        .pipe(scriptChanel())
        .pipe(gulp.dest(destPaths.styles));
};

pipes.buildAppStyles = function ()
{
    var scriptChanel = lazypipe()
        .pipe(gulpOrder, ['app.css'])
        .pipe(gulpCsslint, {
            'adjoining-classes': false,
            'important': false
        })
        .pipe(gulpCsslint.reporter);

    var scriptChanelProd = lazypipe()
        .pipe(gulpCssnano)
        .pipe(gulpConcat, 'app.min.css');

    return gulp.src([appPaths.base + '/**/*.css'])
        .pipe(gulpFlatten())
        .pipe(gulpCache('styles'))
        .pipe(scriptChanel())
        .pipe(gulpIf(env === 'PRODUCTION', gulpRemember('styles')))
        .pipe(gulpIf(env === 'PRODUCTION', scriptChanelProd()))
        .pipe(gulp.dest(destPaths.styles));
};

pipes.buildVendorScripts = function ()
{
    var scriptChanel = lazypipe()
        .pipe(gulpOrder, ['jquery.js', 'jquery.*', 'angular.js', 'angular.*'])
        .pipe(function() {
            return gulpIf(env === 'PRODUCTION', gulpUglify());
        })
        .pipe(function() {
            return gulpIf(env === 'PRODUCTION', gulpConcat('vendor.min.js'));
        });

    return gulp.src(bowerFiles())
        .pipe(gulpFilter('*.js'))
        .pipe(gulpFlatten())
        .pipe(scriptChanel())
        .pipe(gulp.dest(destPaths.scripts));
};

pipes.buildThemeScripts = function ()
{
    var scriptChanel = lazypipe()
        .pipe(function() {
            return gulpIf(env === 'PRODUCTION', gulpUglify());
        })
        .pipe(function() {
            return gulpIf(env === 'PRODUCTION', gulpConcat('theme.min.js'));
        });

    return gulp.src(appPaths.theme + '/**/*.js')
        .pipe(gulpFlatten())
        .pipe(scriptChanel())
        .pipe(gulp.dest(destPaths.scripts));
};

pipes.buildAppScripts = function ()
{
    var scriptChanel = lazypipe()
        .pipe(gulpOrder, ['app.js', 'templates.js'])
        .pipe(gulpPreprocess, {context: {ENV: env}})
        .pipe(function () { return gulpJscs({fix: false, configPath: './jscs.json'}); })
        .pipe(function () { return gulpJscs.reporter(); })
        .pipe(gulpJshint)
        .pipe(gulpJshint.reporter, 'jshint-stylish');

    var scriptChanelProd = lazypipe()
        .pipe(gulpUglify)
        .pipe(gulpConcat, 'app.min.js');

    var partialChanel = lazypipe()
        .pipe(gulpHtmlhint, {'doctype-first': false})
        .pipe(gulpHtmlhint.reporter, 'htmlhint-stylish')
        .pipe(function () {
            return gulpIf(
                env === 'PRODUCTION',
                gulpHtmlmin({collapseWhitespace: true, removeComments: true}
                )
            );
        });

    var templateCache = gulp.src([
            appPaths.base + '/**/*.html',
            '!' + appPaths.base + '/index.html'
        ])
        .pipe(partialChanel())
        .pipe(gulpAngularTplCache(
            'templates.js',
            {standalone: true, module: 'app.templates', root: '/'}
        ));

    var appScripts = gulp.src([appPaths.base + '/**/*.js']);

    return es.merge(appScripts, templateCache)
        .pipe(gulpFlatten())
        .pipe(gulpCache('scripts'))
        .pipe(scriptChanel())
        .pipe(gulpIf(env === 'PRODUCTION', gulpRemember('scripts')))
        .pipe(gulpIf(env === 'PRODUCTION', scriptChanelProd()))
        .pipe(gulp.dest(destPaths.scripts));
};

pipes.buildAppIndex = function() {
    var buildVendorStyles =  pipes.buildVendorStyles();
    var buildThemeStyles =  pipes.buildThemeStyles();
    var buildAppStyles =  pipes.buildAppStyles();
    var buildVendorScripts =  pipes.buildVendorScripts();
    var buildThemeScripts =  pipes.buildThemeScripts();
    var buildAppScripts = pipes.buildAppScripts();

    return gulp.src(appPaths.index)
        .pipe(gulp.dest(destPaths.base))
        .pipe(gulpInject(buildVendorStyles, {relative: true, name: 'vendor'}))
        .pipe(gulpInject(buildThemeStyles, {relative: true, name: 'theme'}))
        .pipe(gulpInject(buildAppStyles, {relative: true, name: 'app'}))
        .pipe(gulpInject(buildVendorScripts, {relative: true, name: 'vendor'}))
        .pipe(gulpInject(buildThemeScripts, {relative: true, name: 'theme'}))
        .pipe(gulpInject(buildAppScripts, {relative: true, name: 'app'}))
        .pipe(gulpIf(
            env === 'PRODUCTION',
            gulpHtmlmin({collapseWhitespace: true, removeComments: true})
        ))
        .pipe(gulp.dest(destPaths.base));
};

pipes.buildAppFonts = function() {
    return gulp.src(appPaths.fonts + '/**/fonts/*')
        .pipe(gulpFlatten())
        .pipe(gulp.dest(destPaths.fonts));
};

pipes.buildAppImages = function() {
    return gulp.src(appPaths.images + '/**/*')
        .pipe(gulp.dest(destPaths.images));
};

pipes.buildApp = function () {
    var def = Q.defer();

    es.merge(
        pipes.buildAppIndex(),
        pipes.buildAppFonts(),
        pipes.buildAppImages()
        )
        .on('error', def.reject)
        .on('end', def.resolve);

    return def.promise;
};

// == HELPERS ========
var cleanDistFolder = function() {
    var deferred = Q.defer();
    del(distFolder, function () {
        deferred.resolve();
    });
    return deferred.promise;
};

var setPaths = function ($path) {
    destPaths = {
        base: $path,
        fonts: $path + '/fonts',
        images: $path + '/images',
        scripts: $path + '/scripts',
        styles: $path + '/styles',
        index: $path + '/index.html'
    };
};

var runServer = function () {
    gulpNodemon({
        script: 'server.js',
        ext: 'js',
        watch: ['devServer/'],
        env: {NODE_DIR : distFolder}
    })
        .on('restart', function () {
            console.log('[nodemon] restarted dev server');
        });

    // start live-reload server
    gulpLivereload.listen({start: true});

    //watch index
    gulp.watch(appPaths.index, function() {
        return pipes.buildAppIndex()
            .pipe(gulpLivereload());
    });

    // watch images
    gulp.watch([appPaths.images + '/**/*'], function() {
        return pipes.buildAppImages()
            .pipe(gulpLivereload());
    });

    // watch html partials
    gulp.watch([appPaths.base + '/**/*.html', '!' + appPaths.base + '/index.html'], function() {
        return pipes.buildAppScripts()
            .pipe(gulpLivereload());
    });

    // watch app scripts
    gulp.watch([appPaths.base + '/**/*.js', '!' + appPaths.theme + '/**/*.js'], function() {
        return pipes.buildAppScripts()
            .pipe(gulpLivereload());
    });

    // watch styles
    gulp.watch([
            appPaths.base + '/**/*.css',
            '!' + appPaths.theme + '/**/*.css'
        ],
        function() {
            return pipes.buildAppStyles()
                .pipe(gulpLivereload());
        }
    );
};

function startPlatoVisualizer(done) {
    var plato = require('plato');

    var options = {
        title: 'Plato Inspections Report'
    };
    var outputDir = 'report/plato';
    var files = appPaths.base + '/**/*.js';

    plato.inspect(files, outputDir, options, platoCompleted);

    function platoCompleted() {
        if (done) {
            done();
        }
    }
}

// == TASKS ========
gulp.task('plato', function(done) {
    startPlatoVisualizer(done);
});

gulp.task('build-dev', function () {
    distFolder = './dev';
    env = 'DEV';

    setPaths(distFolder);

    return cleanDistFolder().then(function () {
        pipes.buildApp();
    });

});

gulp.task('watch-dev', function() {
    distFolder = './dev';
    env = 'DEV';

    setPaths(distFolder);

    return cleanDistFolder().then(function () {
        pipes.buildApp().then(function () {
            runServer();
        });
    });

});

gulp.task('build-prod', function () {
    distFolder = './prod';
    env = 'PRODUCTION';

    setPaths(distFolder);

    return cleanDistFolder().then(function () {
        pipes.buildApp();
    });
});

gulp.task('watch-prod', function() {
    distFolder = './prod';
    env = 'PRODUCTION';

    setPaths(distFolder);

    return cleanDistFolder().then(function () {
        pipes.buildApp().then(function () {
            runServer();
        });
    });

});

// default task builds for prod
gulp.task('default', ['watch-dev']);
