'use strict';

var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');

module.exports = yeoman.generators.Base.extend({

    prompting: function () {
        var done = this.async();

        // Have Yeoman greet the user.
        this.log(yosay(
            'Welcome to the awesome ' + chalk.red('generator-aax') + ' generator!'
        ));

        this.prompt({
            type: 'input',
            name: 'name',
            message: 'Your project name',
            default: this.appname // Default to current folder name
        }, function (answers) {
            this.props = answers
            this.log(answers.name);
            done();
        }.bind(this));
    },

    config: function () {
        this.fs.copyTpl(
            this.templatePath('_bower.json'),
            this.destinationPath('bower.json'), {
                name: this.props.name
            }
        );

        this.fs.copy(
            this.templatePath('_devServer/_routes.js'),
            this.destinationPath('devServer/routes.js')
        );

        this.fs.copy(
            this.templatePath('_editorconfig'),
            this.destinationPath('.editorconfig')
        );

        this.fs.copy(
            this.templatePath('_env'),
            this.destinationPath('.env')
        );

        this.fs.copy(
            this.templatePath('_eslintrc'),
            this.destinationPath('.eslintrc')
        );

        this.fs.copy(
            this.templatePath('_gitignore'),
            this.destinationPath('.gitignore')
        );

        this.fs.copy(
            this.templatePath('_gulpfile.js'),
            this.destinationPath('gulpfile.js')
        );

        this.fs.copy(
            this.templatePath('_jscs.json'),
            this.destinationPath('jscs.json')
        );

        this.fs.copy(
            this.templatePath('_jshintrc'),
            this.destinationPath('.jshintrc')
        );

        this.fs.copy(
            this.templatePath('_jshintignore'),
            this.destinationPath('.jshintignore')
        );

        this.fs.copy(
            this.templatePath('_karma.conf.js'),
            this.destinationPath('karma.conf.js')
        );

        this.fs.copyTpl(
            this.templatePath('_package.json'),
            this.destinationPath('package.json'), {
                name: this.props.name
            }
        );

        this.fs.copy(
            this.templatePath('_README.md'),
            this.destinationPath('README.md')
        );

        this.fs.copy(
            this.templatePath('_server.js'),
            this.destinationPath('server.js')
        );

        this.bulkDirectory('_bootstrap_theme', 'bootstrap_theme');

    },

    //Copy application files
    app: function () {
        this.fs.copyTpl(
            this.templatePath('_app/_config/_configure.config.js'),
            this.destinationPath('app/config/configure.config.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_config/_exception.config.js'),
            this.destinationPath('app/config/exception.config.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_config/_loading_bar.config.js'),
            this.destinationPath('app/config/loading_bar.config.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_config/_ontimeApi.config.js'),
            this.destinationPath('app/config/ontimeApi.config.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_config/_routes.config.js'),
            this.destinationPath('app/config/routes.config.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_config/_toastr.config.js'),
            this.destinationPath('app/config/toastr.config.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_directives/_gitkeep'),
            this.destinationPath('app/directives/.gitkeep')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_features/_footer/_footer.controller.js'),
            this.destinationPath('app/features/footer/footer.controller.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_features/_footer/_footer.html'),
            this.destinationPath('app/features/footer/footer.html')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_features/_footer/_footer.css'),
            this.destinationPath('app/features/footer/footer.css'), {
                name: this.props.name
            }
        );

        this.fs.copyTpl(
            this.templatePath('_app/_features/_header/_header.controller.js'),
            this.destinationPath('app/features/header/header.controller.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_features/_header/_header.html'),
            this.destinationPath('app/features/header/header.html')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_features/_header/_header.css'),
            this.destinationPath('app/features/header/header.css'), {
                name: this.props.name
            }
        );

        this.fs.copyTpl(
            this.templatePath('_app/_features/_landing/_landing.controller.js'),
            this.destinationPath('app/features/landing/landing.controller.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_features/_landing/_landing.html'),
            this.destinationPath('app/features/landing/landing.html')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_features/_landing/_landing.css'),
            this.destinationPath('app/features/landing/landing.css'), {
                name: this.props.name
            }
        );

        this.fs.copyTpl(
            this.templatePath('_app/_features/_landing/_landing.route.js'),
            this.destinationPath('app/features/landing/landing.route.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_features/_ontimeregistration/_ontimeregistration.controller.js'),
            this.destinationPath('app/features/ontimeregistration/ontimeregistration.controller.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_features/_ontimeregistration/_ontimeregistration.html'),
            this.destinationPath('app/features/ontimeregistration/ontimeregistration.html')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_features/_ontimeregistration/_ontimeregistration.css'),
            this.destinationPath('app/features/ontimeregistration/ontimeregistration.css'), {
                name: this.props.name
            }
        );

        this.fs.copyTpl(
            this.templatePath('_app/_features/_ontimeregistration/_ontimeregistration.route.js'),
            this.destinationPath('app/features/ontimeregistration/ontimeregistration.route.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_filters/_capitalize.filter.js'),
            this.destinationPath('app/filters/capitalize.filter.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_filters/_htmltoplaintext.filter.js'),
            this.destinationPath('app/filters/htmltoplaintext.filter.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_filters/_ucfirst.filter.js'),
            this.destinationPath('app/filters/ucfirst.filter.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_services/_API.service.js'),
            this.destinationPath('app/services/API.service.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_services/_exception.service.js'),
            this.destinationPath('app/services/exception.service.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_services/_logger.service.js'),
            this.destinationPath('app/services/logger.service.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_styles/_app.css'),
            this.destinationPath('app/styles/app.css'), {
                name: this.props.name
            }
        );

        this.bulkDirectory('_app/_images', 'app/images');

        this.fs.copyTpl(
            this.templatePath('_app/_404.html'),
            this.destinationPath('app/404.html')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_app.js'),
            this.destinationPath('app/app.js')
        );

        this.fs.copyTpl(
            this.templatePath('_app/_index.html'),
            this.destinationPath('app/index.html'), {
                name: this.props.name
            }
        );

        this.fs.copyTpl(
            this.templatePath('_tests/_unit/_config/_exception.config.spec.js'),
            this.destinationPath('tests/unit/config/exception.config.spec.js')
        );

        this.fs.copyTpl(
            this.templatePath('_tests/_unit/_features/_footer.controller.spec.js'),
            this.destinationPath('tests/unit/features/footer.controller.spec.js')
        );

        this.fs.copyTpl(
            this.templatePath('_tests/_unit/_features/_header.controller.spec.js'),
            this.destinationPath('tests/unit/features/header.controller.spec.js')
        );

        this.fs.copyTpl(
            this.templatePath('_tests/_unit/_features/_landing.controller.spec.js'),
            this.destinationPath('tests/unit/features/landing.controller.spec.js')
        );

        this.fs.copyTpl(
            this.templatePath('_tests/_unit/_features/_landing.route.spec.js'),
            this.destinationPath('tests/unit/features/landing.route.spec.js')
        );

        this.fs.copyTpl(
            this.templatePath('_tests/_unit/_filters/_capitalize.filter.spec.js'),
            this.destinationPath('tests/unit/filters/capitalize.filter.spec.js')
        );

        this.fs.copyTpl(
            this.templatePath('_tests/_unit/_filters/_ucfirst.filter.spec.js'),
            this.destinationPath('tests/unit/filters/ucfirst.filter.spec.js')
        );

    },
    install: function () {
        this.config.save();
        this.installDependencies();
    }
});
