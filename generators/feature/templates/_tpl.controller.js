(function () {
    'use strict';

    angular
        .module('app.controllers')
        .controller('<%= uName %>Controller', <%= uName %>Controller);

    <%= uName %>Controller.$inject = [];

    function <%= uName %>Controller () {
        var vm = this;

        vm.message = 'welcome to your <%= name %> feature';
        //
    }

})();
