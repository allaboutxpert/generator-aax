(function () {
    'use strict';

    angular
        .module('app.routes')
        .config(<%= uName %>Router);

    <%= uName %>Router.$inject = ['$stateProvider'];
    /* @ngInject */
    function <%= uName %>Router($stateProvider) {
        $stateProvider
            .state('app.<%= name %>', {
                url: '/<%= name %>',
                data: {},
                views: {
                    'main@': {
                        templateUrl: '/features/<%= name %>/<%= name %>.html',
                        controller: '<%= uName %>Controller',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    // Constant title
                    $title: function() { return '<%= uName %>'; }
                }
            });
    }

})();
