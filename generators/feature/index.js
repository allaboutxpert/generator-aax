'use strict';

var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var generators = yeoman.generators;
var yosay = require('yosay');
var chalk = require('chalk');
var upperCamelCase = require('uppercamelcase');
var camelCase = require('camelcase');

module.exports = yeoman.generators.Base.extend({

  constructor: function() {
        // arguments and options should be
        // defined in the constructor.
        generators.Base.apply(this, arguments);

        this.argument('name', { type: String, required: true });
        this.uName = upperCamelCase(this.name);
        this.name = camelCase(this.name).toLowerCase();

    },

  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the well-made ' + chalk.red('generator-aax') + ' generator!'
    ));

    this.prompt({
      type    : 'input',
      name    : 'name',
      message : 'Your feature name',
      default : this.name // Default to current folder name
    }, function (answers) {
      this.props = answers
      this.log(answers.name);
      done();
    }.bind(this));

  },

  writing: function () {
    this.fs.copyTpl(
      this.templatePath('_tpl.controller.js'),
      this.destinationPath('app/features/' + this.name + '/' + this.name + '.controller.js'), {
        name: this.name,
        uName: this.uName
      }
    );

    this.fs.copyTpl(
      this.templatePath('_tpl.css'),
      this.destinationPath('app/features/' + this.name + '/' + this.name + '.css'), {
        name: this.name,
        uName: this.uName,
        appName: this.appname
      }
    );

    this.fs.copyTpl(
      this.templatePath('_tpl.html'),
      this.destinationPath('app/features/' + this.name + '/' + this.name + '.html'), {
        name: this.name,
        uName: this.uName
      }
    );

    this.fs.copyTpl(
      this.templatePath('_tpl.route.js'),
      this.destinationPath('app/features/' + this.name + '/' + this.name + '.route.js'), {
        name: this.name,
        uName: this.uName
      }
    );

      this.fs.copyTpl(
          this.templatePath('_gitkeep'),
          this.destinationPath('app/images/' + this.name + '/' + '.gitkeep')
      );

  }
  // install: function () {
  //   this.installDependencies();
  // }
});
