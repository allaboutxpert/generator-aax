(function () {
    'use strict';

    angular.module('app.directives').directive('<%= name %>', <%= name %>Definition);

    function <%= name %>Definition() {

        var directive = {
            restrict: 'EA',
            templateUrl: '/directives/<%= name %>/<%= name %>.html',
            controller: '<%= uName %>Controller',
            controllerAs: 'vm',
            scope: {},
            bindToController: true
        };
        return directive;
    }
})();
