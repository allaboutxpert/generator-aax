(function () {
    'use strict';

    angular
        .module('app.config')
        .config(<%= name %>);

    <%= name %>.$inject = [];

    function <%= name %>() {
        //
    }

})();
