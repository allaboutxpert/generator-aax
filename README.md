# generator-aax 
> Yeoman generator for aaX angularJS development 

## Installation

If this is your first Yeoman project, install Yeoman, Bower & Gulp  using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
sudo npm install -g yo
sudo npm install -g bower
sudo npm install -g gulp
```

Now get the generator from Bitbucket and link it to Node.

```bash
git clone git@bitbucket.org:allaboutxpert/generator-aax.git
cd generator-aax
npm link
```

Then generate your new project:

```bash
mkdir <projectname>
cd <projectname>
yo aax
```

And then you can generate the following artifacts:
 
```bash
// Config
yo aax:config <name>

// Directive
yo aax:directive <name>

// Feature
yo aax:feature <name>

// Filter
yo aax:filter <name>

// Service
yo aax:service <name>

```
Run your project

```bash
gulp
```
 
## Getting To Know Yeoman

Yeoman has a heart of gold. He's a person with feelings and opinions, but he's very easy to work with. If you think he&#39;s too opinionated, he can be easily convinced. Feel free to [learn more about him](http://yeoman.io/).

## License

 © [Lino Steenkamp]()


[npm-image]: https://badge.fury.io/js/generator-aax.svg
[npm-url]: https://npmjs.org/package/generator-aax
[travis-image]: https://travis-ci.org//generator-aax.svg?branch=master
[travis-url]: https://travis-ci.org//generator-aax
[daviddm-image]: https://david-dm.org//generator-aax.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//generator-aax